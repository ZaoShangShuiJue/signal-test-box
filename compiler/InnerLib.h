/*
@file: InnerLib.h
@author: ZZH
@date: 2022-11-16
@info: 内置库
*/
#pragma once
#include "libFun.h"

EXPORT void fs(pFunCallArg_t pArgs, float* output);
