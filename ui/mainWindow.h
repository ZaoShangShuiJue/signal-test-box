#pragma once
#include "ui_mainwindow.h"

#include <QString>
#include <QDebug>
#include <QMainWindow>
#include <QtCore>
#include <QMessageBox>
#include <QPushButton>
#include <QtCharts/QChart>
#include <QtCharts/QChartView>
#include <QtCharts/QValueAxis>
#include <QtCharts/QLineSeries>

#include "LoggerWindow.h"

class MainWindow: public QMainWindow
{
    Q_OBJECT
private:
    unsigned int sigSuffix;
    const QString sigName = "sig%1";
    QString curItemText;
    static const QRegExp sigNameRule;
    QtCharts::QLineSeries* pSeries;
    QtCharts::QValueAxis* pAxisX, * pAxisY;
    QMenu* pMenu;
    LoggerWindow *logWindow;
    
    static const char* arrayKey;
    static const char* fsKey;
    static const char* fsUnitKey;
    static const char* calPointsKey;

    static const int calPointMax;
    static const int calPointMin;

protected:
    Ui::MainWindow ui;

public:
    static const int signalExpressRole = Qt::UserRole + 2;
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

    static const float fsScale[3];

    void addSignal(void);
    void delSignal(void);

    void enableExpress(void);

    void calculateCurSig(void);

    void itemChanged(QListWidgetItem* item);
    void currentItemChanged(QListWidgetItem* current, QListWidgetItem* previous);

    void importWorkspace(void);
    void exportWorkspace(void);

public slots:
    void on_pCalNum_editingFinished(void);
};
